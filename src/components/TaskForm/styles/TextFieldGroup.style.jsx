import styled from 'styled-components';

// export const TextFieldGroup = styled('div')({
//   display: 'flex',
//   justifyContent: 'space-between',
//   width: '600px',
//   '.MuiFormControl-root': {
//     margin: 0,
//   },
// });

export const TextFieldGroup = styled.div`
  display: flex;
  justify-content: space-between;
  width: 600px;
  .MuiFormControl-root {
    margin: 0;
  }

  @media (max-width: 768px) {
    display: inline-grid;
    > div {
      margin-bottom: 30px;
    }
  }
`;
