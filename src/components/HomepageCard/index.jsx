import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { Card } from './HomeCard.style';

const HomePageCard = ({ type }) => {
  const imgUrl = require(`../../assets/images/${_.upperFirst(type)}.jpg`).default;

  return (
    <Card>
      <img className="cardImg" src={imgUrl} alt={type} />
      <div className="cardLayer"></div>
      <h2 className="cardText">{_.upperFirst(type)}</h2>
    </Card>
  );
};

HomePageCard.propTypes = {
  type: PropTypes.string.isRequired,
};

export default HomePageCard;
