import styled from 'styled-components';

export const TextStyle = styled.div`
  display: flex;
  font-size: 18px;
  font-family: 'Sans-serif';
  text-indent: 50px;
  margin-bottom: 10px;
  text-align: justify;
`;

export const BoxStyle1 = styled.div`
  margin-top: 50px;
`;
export const BoxStyle2 = styled.div`
  margin-top: 40px;
  position: absolute;
  top: 10px;
  right: 10px;
`;
