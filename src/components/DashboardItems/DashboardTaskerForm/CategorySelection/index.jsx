import { React, useState } from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';

const CategorySelection = ({ title }) => {
  const [selection, setSelection] = useState();
  const menuItems = ['cleaning', 'removal', 'handyperson'];
  const handleChange = (e) => {
    e.preventDefault();
    setSelection(e.target.value);
  };
  return (
    <Box sx={{ display: 'flex', flexDirection: 'row', margin: '30px' }}>
      <Typography sx={{ display: 'flex', marginRight: '20px', width: '150px' }}>Category</Typography>
      <Select
        labelId="category"
        name="category"
        value={selection}
        label="Category"
        size="small"
        sx={{ width: '450px' }}
        onChange={handleChange}
      >
        {menuItems.map((item, index) => (
          <MenuItem value={item} key={index}>
            {item}
          </MenuItem>
        ))}
      </Select>
    </Box>
  );
};

CategorySelection.propTypes = {
  title: PropTypes.string.isRequired,
};

export default CategorySelection;
