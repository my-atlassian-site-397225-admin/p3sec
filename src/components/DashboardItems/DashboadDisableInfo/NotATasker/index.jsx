import React from 'react';
import Box from '@mui/material/Box';
import Link from '@mui/material/Link';

import CustomButton from '../../../CustomButton';

export default function NoTasker({ link }) {
  return (
    <div>
      <Box>
        <Box
          sx={{
            mt: '200px',
            display: 'flex',
            flexDirection: 'row',
            width: '95%',
            justifyContent: 'center',
            color: '#444',
          }}
        >
          <p> Not a tasker yet!</p>
        </Box>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            width: '95%',
            justifyContent: 'center',
          }}
        >
          <Link underline="none" href={`/${link}`}>
            <CustomButton color="primary" variant="outlined">
              <p>Become a Tasker</p>
            </CustomButton>
          </Link>
        </Box>
      </Box>
    </div>
  );
}
