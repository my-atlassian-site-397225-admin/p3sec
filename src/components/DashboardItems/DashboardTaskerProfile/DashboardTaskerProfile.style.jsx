import styled from 'styled-components';

export const Wrapper = styled.div`
  padding-top: 15px;
  padding-left: 20px;
  width: 80%;
  display: flex;
  flex-direction: column;
  font-family: roboto;
`;

export const LineWrapper = styled.div`
  height: 40px;
  width: 100%;
  padding-top: 10px;
  display: flex;
  text-align: left;
`;

export const Label = styled.div`
  width: 200px;
  font-size: 20px;
`;

export const CategoryLabel = styled(Label)`
  padding-top: 15px;
`;

export const Category = styled.div`
  width: 200px;
  background-color: gray;
  border-radius: 4px;
  margin-left: 4px;
`;

export const Certificate = styled.div`
  width: 400px;
  background-color: gray;
  border-radius: 4px;
  margin-left: 4px;
`;

export const TextArea = styled.div`
  height: 120px;
  width: 100%;
  font-size: 17px;
`;
