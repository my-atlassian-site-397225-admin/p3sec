import React from 'react';
import { ReactSVG } from 'react-svg';
import { LoaderContainer } from './LoadingPage.style';

const LoadingPage = () => (
  <section className="loadingPage">
    <LoaderContainer>
      <ReactSVG
        loading={() => (
          <svg>
            <circle cx="70" cy="70" r="50"></circle>
          </svg>
        )}
        src="svg.svg"
      />
    </LoaderContainer>
  </section>
);

export default LoadingPage;
