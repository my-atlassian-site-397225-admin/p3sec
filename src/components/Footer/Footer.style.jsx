import styled from 'styled-components';
export const Box = styled.div`
  background: #2b6777;
  font-weight: 200;
  font-family: 'Roboto';
  width: 100%;
  height: 260px;
`;
export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  max-width: 1000px;
  margin: 0 auto;
`;
export const Column = styled.div`
  width: 300px;
  display: flex;
  flex-direction: column;
`;
export const Row = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
  grid-gap: 50px;
  @media (max-width: 1000px) {
    grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
  }
`;

export const FooterLink = styled.a`
  color: #fff;
  margin-bottom: 20px;
  font-size: 18px;
  text-decoration: none;
  &:hover {
    color: green;
    transition: 200ms ease-in;
  }
`;

export const Heading = styled.p`
  font-size: 1.3rem;
  color: #fff;
  margin-bottom: 40px;
  font-weight: 300;
`;

export const Bottom = styled.p`
  font-size: 12px;
  color: #fff;
  margin-bottom: 5px;
  position: relative;
  text-align: center;
  top: 50px;
`;
