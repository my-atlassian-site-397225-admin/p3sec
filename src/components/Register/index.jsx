import React, { useRef, useEffect, useCallback, useState } from 'react';
import { useSpring, animated } from 'react-spring';
import PropTypes from 'prop-types';
import TextField from '@mui/material/TextField';
import CustomButton from '../CustomButton';

import { Wrapper, Form, Background, ModalWrapper, ModalContent, CloseModalButton, Text } from './Register.style.jsx';

const RegisterModal = ({ showModal, setShowModal, openLoginModal }) => {
  const modalRef = useRef();

  const [userInfo, setUserInfo] = useState({
    email: '',
    password: '',
    username: '',
    repassword: '',
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    if (userInfo.password !== userInfo.repassword) {
      alert("password doesn't match!");
    } else {
      //TODO: get the id from backend, change this part later
      localStorage.setItem('userID', userInfo.username);
    }
  };

  const handleChange = (e) => {
    e.preventDefault();
    setUserInfo({ [e.target.name]: e.target.value });
  };

  const animation = useSpring({
    config: {
      duration: 250,
    },
    opacity: showModal ? 1 : 0,
    transform: showModal ? `translateY(0%)` : `translateY(-100%)`,
  });

  const closeModal = (e) => {
    if (modalRef.current === e.target) {
      setShowModal(false);
    }
  };

  const keyPress = useCallback(
    (e) => {
      if (e.key === 'Escape' && showModal) {
        setShowModal(false);
      }
    },
    [setShowModal, showModal]
  );

  useEffect(() => {
    document.addEventListener('keydown', keyPress);
    return () => document.removeEventListener('keydown', keyPress);
  }, [keyPress]);
  //TODO: maybe add use google, facebook to register
  return (
    <>
      {showModal ? (
        <Background onClick={closeModal} ref={modalRef}>
          <animated.div style={animation}>
            <ModalWrapper showModal={showModal}>
              <ModalContent>
                <Wrapper>
                  <Form onSubmit={handleSubmit}>
                    <TextField
                      type="email"
                      name="email"
                      placeholder="Type your email here..."
                      value={userInfo.email}
                      onChange={handleChange}
                    />
                    <br />
                    <TextField
                      type="password"
                      name="password"
                      placeholder="Enter your password..."
                      value={userInfo.password}
                      onChange={handleChange}
                    />
                    <br />
                    <TextField
                      type="password"
                      name="repassword"
                      placeholder="Re-enter your password..."
                      value={userInfo.repassword}
                      onChange={handleChange}
                    />
                    <br />
                    <TextField
                      type="text"
                      name="username"
                      placeholder="Type your username here..."
                      value={userInfo.username}
                      onChange={handleChange}
                    />
                    <br />
                    <CustomButton variant="contained" size="large" type="submit">
                      Register
                    </CustomButton>
                    <br />
                    <Text>
                      Already have an account?
                      <CustomButton color="black" size="small" onClick={openLoginModal}>
                        here
                      </CustomButton>
                    </Text>
                  </Form>
                </Wrapper>
              </ModalContent>
              <CloseModalButton aria-label="Close modal" onClick={() => setShowModal((prev) => !prev)} />
            </ModalWrapper>
          </animated.div>
        </Background>
      ) : null}
    </>
  );
};

RegisterModal.propTypes = {
  showModal: PropTypes.bool,
  setShowModal: PropTypes.func,
  openLoginModal: PropTypes.func,
};

export default RegisterModal;
