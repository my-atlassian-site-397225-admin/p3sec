import * as React from 'react';
import PropTypes from 'prop-types';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import { createTheme, ThemeProvider } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: '#2B6777',
    },
    secondary: {
      main: '#C8D8E4',
    },
    success: {
      main: '#438D7D',
    },
  },
});

const TaskStatusTag = ({ ...props }) => (
  <Stack direction="row" spacing={2}>
    <ThemeProvider theme={theme}>
      <Chip {...props} sx={{ width: '80px', fontSize: '1em' }} />
    </ThemeProvider>
  </Stack>
);

TaskStatusTag.propTypes = {
  props: PropTypes.node,
};

export default TaskStatusTag;
