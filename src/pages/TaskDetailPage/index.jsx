import React from 'react';

import TaskDetailCard from '../../components/TaskDetailCard';

const TaskDetailPage = () => <TaskDetailCard />;

export default TaskDetailPage;
