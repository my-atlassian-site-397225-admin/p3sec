import React from 'react';
import SearchBar from '../../../components/SearchBar';
import CustomButton from '../../../components/CustomButton';
import { Box, Container, SearchBarStyle, PostTaskStyle } from './HomePageInfo.style';

const HomePageInfo = () => (
  <Box>
    <Container>
      <PostTaskStyle>
        <h3>Need a hand?</h3>
        <CustomButton color="primary" variant="contained" size="large">
          Post your task for free
        </CustomButton>
      </PostTaskStyle>

      <SearchBarStyle>
        <h3>Want to earn money as a Tasker?</h3>
        <SearchBar placeholder="Enter location to see tasks nearby" width="390px" />
      </SearchBarStyle>
    </Container>
  </Box>
);

export default HomePageInfo;
