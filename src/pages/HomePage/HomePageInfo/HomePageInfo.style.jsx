import styled from 'styled-components';
import HomePageInfo from '../../../assets/images/HomePageInfo.jpg';
export const Box = styled.div`
  width: 100vw;
  height: 91vh;
  font-family: 'Roboto';
  color: White;
  font-spacing: 1%;
  overflow-x: hidden;
  h3 {
    font-weight: 300;
    font-size: 1.5rem;
    margin-bottom: 1rem;
  }
  background: linear-gradient(to right bottom, rgba(68, 68, 68, 0.7), rgba(68, 68, 68, 0.7)), url(${HomePageInfo});
  background-repeat: no-repeat;
  background-size: cover;
`;
export const Container = styled.div`
  display: flex;
  position: relative;
  width: 50%;
  top: 40%;
  left: 50%;
  transform: translate(-50%, -50%);
`;
export const PostTaskStyle = styled.div`
  padding: 0.3rem 1.6rem;
  color: White;
  font-size: 1rem;
  button {
    margin-top: 0;
  }
`;
export const SearchBarStyle = styled.div`
  padding-left: 180px;
  h3 {
    margin-bottom: 1rem;
  }
`;
