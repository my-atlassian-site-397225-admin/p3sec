import { keyframes } from 'styled-components';

export const loadingAnimation = keyframes`
    0% {
        stroke-dashoffset: 440;
    }

    100% {
      stroke-dashoffset: 440;
    }

    50% {
      stroke-dashoffset: 0;
    }

    50.1% {
      stroke-dashoffset: 880;
    }
`;

export const loadingRotate = keyframes`
    from {
        transform: rotate(0deg);
    }

    to {
        transform: rotate(360deg);
    }
`;
